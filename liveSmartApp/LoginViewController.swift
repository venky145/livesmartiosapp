//
//  ViewController.swift
//  liveSmartApp
//
//  Created by Venkatesh Mandapati on 19/09/20.
//  Copyright © 2020 liveSmart. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        self.emailTextField.addBottomBorder() //Add bottom line to text field
        self.passwordTextField.addBottomBorder()
        self.loginButton.layer.cornerRadius = 4.0
    }

    //Login Action
    @IBAction func loginAction(_ sender: Any) {
        if isUserFieldsValid() {
            print("success login")
            self.navigateToNextViewController();
        }else{
            print("Email / Password fields should not be empty");
        }
    }

    //Validate Text fields Email and Password (legth should be more than 0)
    func isUserFieldsValid() -> Bool {
        return (emailTextField.text!.count > 0 && passwordTextField.text!.count > 0)
    }

    func navigateToNextViewController() {
        let mainStoryBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main);

        guard let navigateVC = mainStoryBoard.instantiateViewController(withIdentifier: "ManageNavigationController") as? UINavigationController else {
            return
        }
        navigateVC.modalPresentationStyle = .fullScreen
        present(navigateVC, animated: true, completion: nil)
    }
}

// MARK:- Extensions

// MARK:- UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == emailTextField && emailTextField.text!.count > 0) {
            passwordTextField.becomeFirstResponder()
        }else if(textField == passwordTextField){
            self.view.endEditing(true);
            return false
        }
        return true
    }
}

// MARK:- UITextField
//Custom functionality for UITextField
extension UITextField {
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.black.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
}

