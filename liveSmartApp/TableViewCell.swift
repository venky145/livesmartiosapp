//
//  TableViewCell.swift
//  liveSmartApp
//
//  Created by Venkatesh Mandapati on 21/09/20.
//  Copyright © 2020 liveSmart. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
