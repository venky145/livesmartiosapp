//
//  AddManageViewController.swift
//  liveSmartApp
//
//  Created by Venkatesh Mandapati on 20/09/20.
//  Copyright © 2020 liveSmart. All rights reserved.
//

import UIKit

class AddManageViewController: UIViewController {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var addImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Manage Visitors"
        //Add gesture to imageview for adding agents
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        userImageView.addGestureRecognizer(tapGestureRecognizer)
        addImageView.addGestureRecognizer(tapGestureRecognizer)
    }

    //add User agent action tap gesture
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        print("Image tapped")
        self.performSegue(withIdentifier: "AddVisitorSegue", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
