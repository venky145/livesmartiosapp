//
//  ViewController.swift
//  liveSmartApp
//
//  Created by Venkatesh Mandapati on 19/09/20.
//  Copyright © 2020 liveSmart. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.emailTextField.addBottomBorder()
        self.passwordTextField.addBottomBorder()
        self.loginButton.layer.cornerRadius = 4.0
    }

    @IBAction func loginAction(_ sender: Any) {
        if isUserFieldsValid() {
            print("success login");
        }else{
            print("Email / Password fields should not be empty");
        }
    }
    func isUserFieldsValid() -> Bool {
        return (emailTextField.text!.count > 0 && passwordTextField.text!.count > 0)
    }

}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == emailTextField && emailTextField.text!.count > 0) {
            passwordTextField.becomeFirstResponder()
        }else if(textField == passwordTextField){
            self.view.endEditing(true);
            return false
        }
        return true
    }
}

extension UITextField {
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.black.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
}

