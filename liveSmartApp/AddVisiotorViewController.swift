//
//  AddVisiotorViewController.swift
//  liveSmartApp
//
//  Created by Venkatesh Mandapati on 20/09/20.
//  Copyright © 2020 liveSmart. All rights reserved.
//

import UIKit

class AddVisiotorViewController: UIViewController {

    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var visitoNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!

    @IBOutlet weak var startDateField: UITextField!
    @IBOutlet weak var endDateField: UITextField!
    @IBOutlet weak var startTimeField: UITextField!
    @IBOutlet weak var endTimeField: UITextField!
    @IBOutlet weak var visitorFieldInfoView: UIView!

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var apartmentTableView: UITableView!
    
    @IBOutlet weak var tableiewHeight: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        infoView.layer.cornerRadius = 6.0
        infoView.layer.borderWidth = 1.5
        infoView.layer.borderColor = UIColor(red: 228/255, green: 205/255, blue: 163/255, alpha: 1).cgColor;
        infoView.layer.masksToBounds = true
    }

    override func viewDidAppear(_ animated: Bool) {
        self.startDateField.addBottomBorder()
        self.endDateField.addBottomBorder()
        self.startTimeField.addBottomBorder()
        self.endTimeField.addBottomBorder()
        self.emailField.addBottomBorder()
        self.visitoNameField.addBottomBorder()
    }

//    override func viewWillAppear(_ animated: Bool) {
//        self.apartmentTableView.addObserver(self, forKeyPath: "tableContentSize", options: .new, context: nil);
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        self.apartmentTableView.removeObserver(self, forKeyPath: "tableContentSize");
//    }
//
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        if keyPath == "tableContentSize"{
//            if let value = change?[.newKey] {
//                let size = value as! CGSize
//                self.tableiewHeight.constant = size.height;
//            }
//        }
//
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:- UITextFieldDelegate
extension AddVisiotorViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.view.endEditing(true);
            return false
    }
}

extension AddVisiotorViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell{
            cell.textLabel?.text = "\(indexPath.row)"
            return cell
        }
        return UITableViewCell()
    }


}
